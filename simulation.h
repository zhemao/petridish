#ifndef __PETRIDISH_SIMULATION_H__
#define __PETRIDISH_SIMULATION_H__

#include "vector.h"

#define WIDTH 80
#define HEIGHT 23
#define POPULATION_CAP WIDTH * HEIGHT

vector * arena;
int births;
int deaths;
int stages;

void move_all();
void interact_all();
void kill_elderly();
void update_arena();
void print_arena();

#endif /* __PETRIDISH_SIMULATION_H__ */

