OPTS=-O2

petri: simulation.h simulation.c vector.o organism.o
	gcc $(OPTS) simulation.c vector.o organism.o -lncurses -o petri

organism.o: organism.h organism.c
	gcc $(OPTS) -c organism.c
	
vector.o: vector.h vector.c
	gcc $(OPTS) -c vector.c

clean:
	rm -f *.o petridish
