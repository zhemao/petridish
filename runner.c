#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char *argv[]){
	int ch;
	int r=0, c=0;
	
	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();
	
	while((ch = getch()) != 'q'){
		mvdelch(r,c);
		if(ch == KEY_LEFT)
			c--;
		else if(ch == KEY_RIGHT)
			c++;
		else if(ch == KEY_UP)
			r--;
		else if(ch == KEY_DOWN)
			r++;
		mvaddch(r,c,'a');
		refresh();
	}
	
	endwin();
	
	return 0;
}
