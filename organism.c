#include "organism.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void random_dna(char * dna){
	int i;
	for(i=0; i<GENOME_SIZE; i++){
		dna[i] = rand() % 16;
	}
}

organism * build_from_dna(char * dna){
	organism * org = (organism*)malloc(sizeof(organism));
	strncpy(org->dna, dna, GENOME_SIZE);
	
	org->age = 0;
	org->gender = (dna[0] | dna[1]) & 1;
	if(org->gender)
		org->ch = 65 + (dna[2] | dna[3]) % 26;
	else org->ch = 97 + (dna[2] | dna[3]) % 26;
	org->speed = (dna[4] | dna[5])/4 + 1;
	org->strength = dna[6] | dna[7];
	org->life_expectancy = (dna[8] | dna[9]) * 10;
	
	return org;
}

organism * mate(organism * orga, organism * orgb){
	assert(orga->gender != orgb->gender);
	assert(neighboring(orga, orgb));

	int success = rand() % 2;
	int i;
	char dna[GENOME_SIZE];
	
	if(!success) return NULL;
	
	for(i=0; i<GENOME_SIZE/2; i++){
		dna[2*i] = orga->dna[2*i + (rand() % 2)];
		dna[2*i+1] = orgb->dna[2*i + (rand() % 2)];
	}
	
	return build_from_dna(dna);
}

int fight(organism * orga, organism * orgb){
	assert(orga->gender == 1 && orga->gender == orgb->gender);
	assert(neighboring(orga, orgb));
	assert(orga->ch != orgb->ch);

	int ascore = (rand() % 16) * orga->strength;
	int bscore = (rand() % 16) * orgb->strength;
	
	if(ascore > bscore) return 1;
	return 0;
}

int neighboring(organism * orga, organism * orgb){
	return abs(orga->x - orgb->x) < 2 && abs(orga->y - orgb->y) < 2;
}

void random_move(organism * org){
	int move = rand() % 4;
	int speed = rand() % org->speed;
	
	switch(move){
	case 0: org->x += speed;
		break;
	case 1: org->x -= speed;
		break;
	case 2: org->y += speed;
		break;
	case 3: org->y -= speed;
	}
}
