#include "simulation.h"
#include "organism.h"
#include <ncurses.h>
#include <assert.h>
#include <string.h>


void shift_array(char * arr, int start, int end){
	int i;
	
	for(i=start; i<end-1; i++){
		arr[i] = arr[i+1];
	}
}

void move_all(){
	int i;
	organism * org;
	for(i=0;i<arena->length;i++){
		org = vector_get(arena, i);
		random_move(org);
		if(org->x > WIDTH) org->x -= WIDTH;
		else if(org->x < 0) org->x += WIDTH;
		if(org->y > HEIGHT) org->y -= HEIGHT;
		else if(org->y < 0) org->y += HEIGHT;
	}
}

void interact_all(){
	int i,j;
	int length = arena->length;
	organism * org, * orga, * orgb;
	char toremove[length];
	
	memset(toremove, 0, length);
	
	for(i=0; i<length-1; i++){
		orga = vector_get(arena, i);
		for(j=i+1; j<length; j++){
			orgb = vector_get(arena, j);
			if(orga && orgb && neighboring(orga, orgb)){
				if(orga->gender == orgb->gender){
					if(orga->gender == 1 && orga->ch != orgb->ch){
						if(fight(orga, orgb))
							toremove[j] = 1;
						else toremove[i] = 1;
						deaths++;
					} 
				}
				else {
					org = mate(orga, orgb);
					if(org){
						org->x = rand() % WIDTH;
						org->y = rand() % HEIGHT;
						vector_add(arena, org, sizeof(*org));
						free(org);
						births++;
					}
				} 
			}
		}
	}
	
	for(i=0; i<length;){
		if(toremove[i]){
			vector_remove(arena, i);
			shift_array(toremove, i, length);
			length--;
		}
		else i++;
	}
	
	while(arena->length > POPULATION_CAP) {
		vector_remove(arena, 0);
		deaths++;
	}
}

void kill_elderly(){
	int i;
	organism * org;

	for(i=0; i<arena->length;){
		org = vector_get(arena, i);
		org->age++;
		if(org->age > org->life_expectancy){ 
			vector_remove(arena, i); 
			deaths++; 
		}
		else i++;
	}
}

void update_arena(){
	move_all();
	interact_all();
	kill_elderly();
}

void print_arena(){
	int i;
	organism * org;
	for(i=0; i<arena->length; i++){
		org = vector_get(arena, i);
		mvaddch(org->y, org->x, org->ch);
	}
	refresh();
}

void print_endstats(){
	printf("Stages: %d\n", stages);
	printf("Births: %d\n", births);
	printf("Deaths: %d\n", deaths); 
	printf("Final Population: %d\n", arena->length);	
}

void init_arena(){
	int i;
	organism * org;
	genome dna;
	
	births = 20;
	deaths = 0;
	
	srand(time(NULL));
	arena = create_vector();
	
	for(i=0; i<births; i++){
		random_dna(dna);
		org = build_from_dna(dna);
		org->x = rand() % WIDTH;
		org->y = rand() % HEIGHT;
		vector_add(arena, org, sizeof(*org));
		free(org);
	}
}

int main(int argc, char *argv[]){
	int ch;
	int r=0, c=0;
	stages = 0;
	
	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();
	
	init_arena();
	print_arena();
	
	while((ch = getch()) != 'q' && arena->length > 0){
		clear();
		update_arena();
		print_arena();
		stages++;
	}
	
	endwin();
	print_endstats();
	
	return 0;
}
