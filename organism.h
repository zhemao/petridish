#ifndef __PETRIDISH_ORGANISM_H__
#define __PETRIDISH_ORGANISM_H__

#define GENOME_SIZE 10

typedef char genome[GENOME_SIZE];

typedef struct {
	int x;
	int y;
	int age;
	int life_expectancy;
	char dna[GENOME_SIZE];
	char ch;
	char gender;
	char strength;
	char speed;
} organism;

void random_dna(char * dna);
organism * build_from_dna(char * dna);
organism * mate(organism * orga, organism * orgb);
int fight(organism * orga, organism * orgb);
int neighboring(organism * orga, organism * orgb);
void random_move(organism * org);

#endif /* __PETRIDISH_ORGANISM_H__ */



